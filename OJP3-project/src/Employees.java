/**
 * Created by Marek on 2016-01-16.
 */
public class Employees extends Persons{
    private int Salary;
    private int WorkHours;
    private int NominalHours;
    private Date DateofEmployment;

    public Employees() {
        Salary = 0;
        WorkHours = 0;
        NominalHours = 0;
        DateofEmployment = new Date();
    }

    public Employees(int salary, int workhours, int nominalhours, int year, int month, int day) {
        Salary = salary;
        WorkHours = workhours;
        NominalHours = nominalhours;
        DateofEmployment = new Date(year, month, day);
    }

    public Employees(String fname, String sname, Date dateofemployment, int salary, int workhours, int nominalhours, Date dateofborn) {
        super(fname,sname,dateofborn);
        Salary = salary;
        WorkHours = workhours;
        NominalHours = nominalhours;
        DateofEmployment = dateofemployment;
    }

    // Set methods
    public void SetSalary(int salary) { Salary = salary; }
    public void SetWorkHours(int workhours) { WorkHours = workhours; }
    public void SetNominalHours(int nominalhours) { NominalHours = nominalhours; }
    public void SetDateofEmployment(Date dateofemployment) { DateofEmployment = dateofemployment; }

    // Get methods
    public int GetSalary() { return Salary; }
    public int GetWorkHours() { return WorkHours; }
    public int GetNominalHours() { return NominalHours;}
    public Date GetDateofEmployment(){ return DateofEmployment;}

    public void ShowEmployees(){
        System.out.println("// About Employee //");
        System.out.println("Salary: " + Salary);
        System.out.println("Work hours: " + WorkHours);
        System.out.println("Nominal hours: " + NominalHours);
        System.out.println("Overtime: " + OverTime());
        System.out.print("Date of employment: ");
        DateofEmployment.ShowDate();
        System.out.println("");
    }

    public int OverTime(){
        if(WorkHours > NominalHours ) { return (WorkHours - NominalHours); }
        else { return 0; }
    }

    public void ShowAllInformation(){
        ShowPerson();
        ShowAddress();
        ShowEmployees();
        System.out.println("////////////////////////////////////");
    }
}

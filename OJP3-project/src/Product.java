import javax.lang.model.element.Name;

/**
 * Created by Marek on 2016-01-18.
 */
public class Product {
    private int IdProduct;
    private int Price;
    private String NameProduct;
    private Date DayofRental;
    private static int CountProduct;

    public Product(String nameporoduct, int price, int day, int month, int year){
        CountProduct++;
        IdProduct = CountProduct;
        NameProduct = nameporoduct;
        Price = price;
        DayofRental = new Date(day,month,year);
    }

    // Set methods
    public void SetNameProduct(String nameproduct){ NameProduct = nameproduct; }
    public void SetPrice(int price){ Price = price; }
    public void SetDayofRental(Date dayofrental){ DayofRental = dayofrental; }

    // Get methods
    public static int GetCountProduct() { return CountProduct; }

    public int GetIdProduct() { return IdProduct; }
    public String GetNameProduct() { return NameProduct; }
    public int GetPrice() { return Price; }
    public Date GetDayofRental() { return DayofRental; }

    protected void finalize(){ CountProduct--; }


}

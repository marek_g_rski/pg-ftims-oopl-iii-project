/**
 * Created by Marek on 2016-01-16.
 */
public class Date {
    private int Year;
    private int Month;
    private int Day;

    public Date(){
        Year = 0;
        Month = 0;
        Day = 0;
    }

    public Date(int year, int month, int day){
        Year = year;
        Month = month;
        Day = day;
    }

    // Set methods
    public void SetYear(int year){ Year = year;}
    public void SetMonth(int month){ Year = month;}
    public void SetDay(int day){ Year = day;}

    // Get methods
    public int GetYear() { return Year; }
    public int GetMonth() { return Month; }
    public int GetDay() { return Day; }

    public void ShowDate(){
        System.out.println(Day + "-" + Month + "-" + Year);
    }
}

/**
 * Created by Marek on 2016-01-16.
 */
public class Persons extends Address{
    private String FName;
    private String SName;
    private Date DateofBorn;

    public Persons(){
        FName = "unknown";
        SName = "unknown";
        DateofBorn = new Date();
    }

    public Persons(String country, String city, String street, int numberhouse, int numberbuilding, int zipcode, String fname, String sname, Date dayofborn){
        super(country, city, street, numberhouse, numberbuilding, zipcode);
        FName = fname;
        SName = sname;
        DateofBorn = dayofborn;
    }

    public Persons(String fname, String sname, Date dayofborn){
        FName = fname;
        SName = sname;
        DateofBorn = dayofborn;
    }

    // Set methods
    public void SetFName(String fname){ FName = fname; }
    public void SetSName(String sname) { SName = sname; }
    public void SetAge(Date dayofBorn) { DateofBorn = dayofBorn; }

    // Get methods
    public String GetFName(){ return FName; }
    public String GetSName() { return SName; }
    public Date GetDateofBorn() { return DateofBorn; }

    //
    public void ShowPerson(){
        System.out.println("// Personal information //");
        System.out.println("First name: " + FName);
        System.out.println("Second name: " + SName);
        System.out.println("Age name: " + DateofBorn);
        System.out.println("");
    }

    public void ShowAllInformation(){
        ShowPerson();
        ShowAddress();
        System.out.println("////////////////////////////////////");
    }
}

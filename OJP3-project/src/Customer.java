/**
 * Created by Marek on 2016-01-18.
 */
import java.util.*;

public class Customer extends Persons{
    private int Id;
    private Map<Integer, Product> ProductList;
    private static int CountPersons;

    public Customer(String fname, String sname, Date dayofborn) {
        super(fname, sname, dayofborn);
        CountPersons++;
        Id = CountPersons;
        ProductList = new HashMap();
    }

    // Get methods
    public int GetCountPersons() { return CountPersons; }
    public int GetId() { return Id; }

    public void AddProductToList(int id, Product product){ ProductList.put(id, product); }
    public Product GetProduct(int id) { return ProductList.get(id); }
    public Map<Integer, Product> GetProductsList() { return ProductList; }
    public void RemoveProduct(int id) { ProductList.remove((id)); }

    protected void finalize () { CountPersons--; }
}

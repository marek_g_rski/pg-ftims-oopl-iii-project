import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;

public class window extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton ProductButton;
    private JButton employeeButton;
    private JButton customersButton;
    private JList list1;
    private JTextField f4;
    private JTextField f1;
    private JTextField f2;
    private JTextField f3;
    private JTextField f5;
    private JTextField f6;
    private JTextField f7;
    private JTextField f8;
    private JButton addButton;
    private JButton deleteButton;
    private JPanel addCustomer;
    private JTextField f9;
    private JTextField f10;
    private JTextField f11;
    private JTextArea textArea1;
    private JButton addProduct;
    private JList list2;
    private JButton cancelButton;
    private JLabel l1;
    private JLabel l2;
    private JLabel l3;
    private JLabel l4;
    private JLabel l5;
    private JLabel l6;
    private JLabel l7;
    private JLabel l8;
    private JLabel l9;
    private JLabel l10;
    private JLabel l11;
    private JTextField f12;
    private JLabel l12;
    private JTextField f13;
    private JTextField f14;
    private JTextField f15;
    private JLabel l13;
    private JLabel l14;
    private JLabel l15;
    private JPanel addproduct;
    private JButton deletep;
    private  List<Customer> CustomersList;
    private  List<Product> ProductsList;
    private  List<Employees>  employeesList;
    private int check;


    public window() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        contentPane.setPreferredSize(new Dimension(800, 500));
        CustomersList = new ArrayList<Customer>();
        ProductsList = new ArrayList<Product>();
        employeesList = new ArrayList<Employees>();

        check = 0;

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        customersButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addProduct.setVisible(false);
                addCustomer.setVisible(false);
                list1.setListData(CustomersList.toArray());
                addButton.setVisible(true);
                addButton.setText("Add customer");
                check = 1;
               }
        });

        ProductButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addProduct.setVisible(false);
                addCustomer.setVisible(false);
                list1.setListData(ProductsList.toArray());
                addButton.setVisible(true);
                addButton.setText("Add product");
                check = 2;


            }
        });

        deletep.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CustomersList.get(list1.getLeadSelectionIndex()).RemoveProduct(list2.getLeadSelectionIndex()-1);
                list2.setListData(CustomersList.get(list1.getLeadSelectionIndex()).GetProductsList().values().toArray());
                deletep.setVisible(false);
            }
        });

        addProduct.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFrame frame = new JFrame("test");
                Object[] possibilities = {"ham", "spam", "yam"};
                Product s = (Product)JOptionPane.showInputDialog(
                        frame,
                        "",
                        "Add product",
                        JOptionPane.PLAIN_MESSAGE,
                        null,
                        ProductsList.toArray(),
                        "");
                CustomersList.get(list1.getLeadSelectionIndex()).AddProductToList(s.GetIdProduct(),s);
                list2.setListData(CustomersList.get(list1.getLeadSelectionIndex()).GetProductsList().values().toArray());
                if(!CustomersList.get(list1.getLeadSelectionIndex()).GetProductsList().isEmpty()){
                    deletep.setVisible(true);
                }
                else {
                    deletep.setVisible(true);
                }
            }
        });

        employeeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addProduct.setVisible(false);
                addCustomer.setVisible(false);
                list1.setListData(employeesList.toArray());
                addButton.setVisible(true);
                addButton.setText("Add employee");
                check = 3;
            }
        });

        addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addProduct.setVisible(false);
                addCustomer.setVisible(true);

                if(check==1) {
                    addCustomer.setVisible(true);

                    l1.setText("First name");
                    f1.setVisible(true);
                    l1.setVisible(true);

                    l2.setText("Second  name");
                    f2.setVisible(true);
                    l2.setVisible(true);

                    l3.setText("Day of born");
                    f3.setVisible(true);
                    l3.setVisible(true);

                    l4.setText("Month of born");
                    f4.setVisible(true);
                    l4.setVisible(true);

                    l5.setText("Year of born");
                    f5.setVisible(true);
                    l5.setVisible(true);

                    l6.setText("Country");
                    f6.setVisible(true);
                    l6.setVisible(true);

                    l7.setText("City");
                    f7.setVisible(true);
                    l7.setVisible(true);

                    l8.setText("Street");
                    f8.setVisible(true);
                    l8.setVisible(true);

                    l9.setText("Number building");
                    f9.setVisible(true);
                    l9.setVisible(true);

                    l10.setText("Number house");
                    f10.setVisible(true);
                    l10.setVisible(true);

                    l11.setText("Zip");
                    f11.setVisible(true);
                    l11.setVisible(true);

                    l12.setText("Salary");
                    f12.setVisible(false);
                    l12.setVisible(false);

                    l13.setText("Day of Employment");
                    f13.setVisible(false);
                    l13.setVisible(false);

                    l14.setText("Month of Employment");
                    f14.setVisible(false);
                    l14.setVisible(false);

                    l15.setText("Year of Employment");
                    f15.setVisible(false);
                    l15.setVisible(false);
            }

                if(check==2) {
                    addCustomer.setVisible(true);

                    l1.setText("Name product");
                    f1.setVisible(true);
                    l1.setVisible(true);

                    l2.setText("Price");
                    f2.setVisible(true);
                    l2.setVisible(true);

                    l3.setText("Day add");
                    f3.setVisible(true);
                    l3.setVisible(true);

                    l4.setText("Month add");
                    f4.setVisible(true);
                    l4.setVisible(true);

                    l5.setText("Year add");
                    f5.setVisible(true);
                    l5.setVisible(true);

                    l6.setText("Country");
                    f6.setVisible(false);
                    l6.setVisible(false);

                    l7.setText("City");
                    f7.setVisible(false);
                    l7.setVisible(false);

                    l8.setText("Street");
                    f8.setVisible(false);
                    l8.setVisible(false);

                    l9.setText("Number building");
                    f9.setVisible(false);
                    l9.setVisible(false);

                    l10.setText("Number house");
                    f10.setVisible(false);
                    l10.setVisible(false);

                    l11.setText("Zip");
                    f11.setVisible(false);
                    l11.setVisible(false);

                    l12.setText("Salary");
                    f12.setVisible(false);
                    l12.setVisible(false);

                    l13.setText("Day of Employment");
                    f13.setVisible(false);
                    l13.setVisible(false);

                    l14.setText("Month of Employment");
                    f14.setVisible(false);
                    l14.setVisible(false);

                    l15.setText("Year of Employment");
                    f15.setVisible(false);
                    l15.setVisible(false);


                }

                if(check==3) {
                    addCustomer.setVisible(true);

                    l1.setText("First name");
                    f1.setVisible(true);
                    l1.setVisible(true);

                    l2.setText("Second  name");
                    f2.setVisible(true);
                    l2.setVisible(true);

                    l3.setText("Day of born");
                    f3.setVisible(true);
                    l3.setVisible(true);

                    l4.setText("Month of born");
                    f4.setVisible(true);
                    l4.setVisible(true);

                    l5.setText("Year of born");
                    f5.setVisible(true);
                    l5.setVisible(true);

                    l6.setText("Country");
                    f6.setVisible(true);
                    l6.setVisible(true);

                    l7.setText("City");
                    f7.setVisible(true);
                    l7.setVisible(true);

                    l8.setText("Street");
                    f8.setVisible(true);
                    l8.setVisible(true);

                    l9.setText("Number building");
                    f9.setVisible(true);
                    l9.setVisible(true);

                    l10.setText("Number house");
                    f10.setVisible(true);
                    l10.setVisible(true);

                    l11.setText("Zip");
                    f11.setVisible(true);
                    l11.setVisible(true);

                    l12.setText("Salary");
                    f12.setVisible(true);
                    l12.setVisible(true);

                    l13.setText("Day of Employment");
                    f13.setVisible(true);
                    l13.setVisible(true);

                    l14.setText("Month of Employment");
                    f14.setVisible(true);
                    l14.setVisible(true);

                    l15.setText("Year of Employment");
                    f15.setVisible(true);
                    l15.setVisible(true);
                }

            }
        });

        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addCustomer.setVisible(false);
                addButton.setVisible(false);
                check = 0;

            }
        });


        list2.addListSelectionListener(new ListSelectionListener() {
            // @Override
            public void valueChanged(ListSelectionEvent e) {
                deletep.setVisible(true);
            }
        });


        list1.addListSelectionListener(new ListSelectionListener() {


            // @Override
            public void valueChanged(ListSelectionEvent e) {
                addProduct.setVisible(true);
                if(check==1) {
                    addproduct.setVisible(true);
                    textArea1.setText("");
                    int d = CustomersList.get((list1.getSelectedIndex())).GetDateofBorn().GetDay();
                    int m = CustomersList.get((list1.getSelectedIndex())).GetDateofBorn().GetMonth();
                    int y = CustomersList.get((list1.getSelectedIndex())).GetDateofBorn().GetYear();
                    textArea1.append("First name: " + CustomersList.get((list1.getSelectedIndex())).GetFName() + "\n");
                    textArea1.append("Second name: " + CustomersList.get((list1.getSelectedIndex())).GetSName()+ "\n");
                    textArea1.append("Born: " + d + "-" + m + "-" + y + "\n");
                    textArea1.append("Country: " + CustomersList.get((list1.getSelectedIndex())).GetCountry()+ "\n");
                    textArea1.append("City: " + CustomersList.get((list1.getSelectedIndex())).GetCity() + "\n");
                    textArea1.append("Street: " + CustomersList.get((list1.getSelectedIndex())).GetStreet() + " " +  CustomersList.get((list1.getSelectedIndex())).GetNumberBuilding() + "/" + CustomersList.get((list1.getSelectedIndex())).GetNumberHouse() + "\n");
                    textArea1.append("Zip code: " + CustomersList.get((list1.getSelectedIndex())).GetZipCode()+ "\n");


                }

                if(check==3) {
                    addproduct.setVisible(true);
                    textArea1.setText("");
                    int d = employeesList.get((list1.getSelectedIndex())).GetDateofBorn().GetDay();
                    int m = employeesList.get((list1.getSelectedIndex())).GetDateofBorn().GetMonth();
                    int y = employeesList.get((list1.getSelectedIndex())).GetDateofBorn().GetYear();
                    textArea1.append("First name: " + employeesList.get((list1.getSelectedIndex())).GetFName() + "\n");
                    textArea1.append("Second name: " + employeesList.get((list1.getSelectedIndex())).GetSName()+ "\n");
                    textArea1.append("Born: " + d + "-" + m + "-" + y + "\n");
                    textArea1.append("Country: " + employeesList.get((list1.getSelectedIndex())).GetCountry()+ "\n");
                    textArea1.append("City: " + employeesList.get((list1.getSelectedIndex())).GetCity() + "\n");
                    textArea1.append("Street: " + employeesList.get((list1.getSelectedIndex())).GetStreet() + " " +  employeesList.get((list1.getSelectedIndex())).GetNumberBuilding() + "/" + employeesList.get((list1.getSelectedIndex())).GetNumberHouse() + "\n");
                    textArea1.append("Zip code: " + employeesList.get((list1.getSelectedIndex())).GetZipCode()+ "\n");
                }


            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
// add your code here
        if(check==1) {
            String Fname = f1.getText();
            String Sname = f2.getText();
            int day = Integer.parseInt(f3.getText());
            int month = Integer.parseInt(f4.getText());
            int year = Integer.parseInt(f5.getText());
            String Country = f6.getText();
            String City = f7.getText();
            String Street = f8.getText();
            int nbuilding = Integer.parseInt(f9.getText());
            int nhouse = Integer.parseInt(f10.getText());
            int zip = Integer.parseInt(f11.getText());

           JFrame f1 = new JFrame("Add product");


            CustomersList.add(new Customer(Fname, Sname, new Date(year, month, day) ));
            list1.setListData(CustomersList.toArray());
            addCustomer.setVisible(false);
        }

        if(check==2) {
            String name = f1.getText();
            int price = Integer.parseInt(f2.getText());
            int day = Integer.parseInt(f3.getText());
            int month = Integer.parseInt(f4.getText());
            int year = Integer.parseInt(f5.getText());

            ProductsList.add(new Product(name, price, day, month, year));
            addCustomer.setVisible(false);
        }

        if(check==3) {
            String Fname = f1.getText();
            String Sname = f2.getText();
            int day = Integer.parseInt(f3.getText());
            int month = Integer.parseInt(f4.getText());
            int year = Integer.parseInt(f5.getText());
            String Country = f6.getText();
            String City = f7.getText();
            String Street = f8.getText();
            int nbuilding = Integer.parseInt(f9.getText());
            int nhouse = Integer.parseInt(f10.getText());
            int zip = Integer.parseInt(f11.getText());
            int salary = Integer.parseInt(f11.getText());
            int dayw = Integer.parseInt(f3.getText());
            int monthw = Integer.parseInt(f4.getText());
            int yearw = Integer.parseInt(f5.getText());

            employeesList.add(new Employees(Fname, Sname, new Date(dayw,monthw,yearw), salary, 0, 0, new Date(day,month,year)));
            addCustomer.setVisible(false);
        }



        f1.setText("");
        f2.setText("");
        f3.setText("");
        f4.setText("");
        f5.setText("");
        f6.setText("");
        f7.setText("");
        f8.setText("");
        f9.setText("");
        f10.setText("");
        f11.setText("");
        f12.setText("");
        f13.setText("");
        f14.setText("");
        f15.setText("");

    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    private void onProduct() {
        dispose();
    }

    public static void main(String[] args) {
        window dialog = new window();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}

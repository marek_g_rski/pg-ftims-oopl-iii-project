/**
 * Created by Marek on 2016-01-16.
 */
public class Address {
    String Country;
    String City;
    String Street;
    int NumberHouse;
    int NumberBuilding;
    int ZipCode;

    public Address() {
        Country = "unknown";
        City = "unknown";
        Street = "unknown";
        NumberHouse = 0;
        NumberBuilding = 0;
        ZipCode = 0;
    }

    public Address(String country, String city, String street, int numberhouse, int numberbuilding, int zipcode){
        Country = country;
        City = city;
        Street = street;
        NumberHouse = numberhouse;
        NumberBuilding = numberbuilding;
        ZipCode = zipcode;
    }

    // Set methods
    public void SetCountry(String country){ Country = country; }
    public void SetCity(String city){ City = city; }
    public void SetStreet(String street) { Country = street; }
    public void SetNumberHouse(int numberhouse){ NumberHouse = numberhouse; }
    public void SetNumberBuilding(int numberbuilding) { NumberBuilding = numberbuilding;}
    public void SetZipCode(int zipcode) {ZipCode = zipcode;}

    // Get methods
    public String GetCountry(){ return Country; }
    public String GetCity(){ return City; }
    public String GetStreet(){ return Street;}
    public int GetNumberHouse(){ return NumberHouse; }
    public int GetNumberBuilding(){return NumberBuilding; }
    public int GetZipCode(){ return ZipCode; }

    public void ShowAddress(){
        System.out.println("// Adress Information //");
        System.out.println("Country: " + Country);
        System.out.println("City: " + City);
        System.out.println("Street: " + Street + " "+ NumberBuilding + "/" + NumberHouse);
        System.out.println("Zip code: " + ZipCode);
        System.out.println("");
    }

}
